package com.sitehands.crucible_plugins.slack;

import com.atlassian.crucible.spi.services.UserService;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.gson.Gson;

public class SlackSettingsStoreImpl implements SlackSettingsStore {
    private static final String KEY = "slackSettings";
    private final UserService userService;
    private final PluginSettingsFactory pluginSettingsFactory;

    public SlackSettingsStoreImpl(UserService userService, PluginSettingsFactory pluginSettingsFactory) {
        this.userService = userService;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public SlackSettings getSettings() {
        String slackSettings = (String) this.pluginSettingsFactory.createGlobalSettings().get("slackSettings");
        return slackSettings != null ? (SlackSettings) (new Gson()).fromJson(slackSettings, SlackSettings.class) : null;
    }

    public void setSettings(SlackSettings settings) {
        String json = null;
        if (settings != null) {
            json = (new Gson()).toJson(settings);
        }

        this.pluginSettingsFactory.createGlobalSettings().put("slackSettings", json);
    }

}
