package com.sitehands.crucible_plugins.slack;

public class SlackSettings {
    private String channelSettings;

    public SlackSettings(String channelSettings) {
        this.channelSettings = channelSettings;
    }

    public SlackSettings() {
    }

    public String getChannelSettings() {
        return channelSettings;
    }

    public void setChannelSettings(String channelSettings) {
        this.channelSettings = channelSettings;
    }

}
