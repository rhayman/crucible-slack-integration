package com.sitehands.crucible_plugins.slack;

public interface SlackSender {
    void sendMessage(SlackMessage var1, SlackChannelSettings var2);
}
