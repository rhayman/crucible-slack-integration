package com.sitehands.crucible_plugins.slack;

public interface SlackPluginComponent {
    String getName();
}
