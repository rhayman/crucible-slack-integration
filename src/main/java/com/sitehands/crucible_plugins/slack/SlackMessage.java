package com.sitehands.crucible_plugins.slack;

import java.text.MessageFormat;

public class SlackMessage {
    private String text;
    private String channel;
    private String username;
    private String icon_url;

    // New fields.
    private String fallback;
    private String pretext;
    private String color;
    private String author_name;
    private String author_link;
    private String author_icon;
    private String title;
    private String title_link;
    private String image_url;
    private String thumb_url;
    private String footer;
    private String footer_icon;
    private int ts;

    public SlackMessage() {
    }

    public static String url(String url, String text) {
        return MessageFormat.format("<{0}|{1}>", new Object[]{url, text});
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon_url() {
        return this.icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }
}
