package com.sitehands.crucible_plugins.slack;

public class SlackChannelSettings {
    private String webhooks;
    private String baseUrl;

    public SlackChannelSettings(String webhooks, String baseUrl) {
        this.webhooks = webhooks;
        this.baseUrl = baseUrl;
    }

    public SlackChannelSettings() {
    }

    public String getWebhooks() {
        return this.webhooks;
    }

    public void setWebhooks(String webhooks) {
        this.webhooks = webhooks;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
