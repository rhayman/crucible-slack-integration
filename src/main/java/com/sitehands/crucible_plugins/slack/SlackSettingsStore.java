package com.sitehands.crucible_plugins.slack;

public interface SlackSettingsStore {
    SlackSettings getSettings();

    void setSettings(SlackSettings var1);
}
