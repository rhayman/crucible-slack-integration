package com.sitehands.crucible_plugins.slack;

import com.atlassian.crucible.event.AllReviewersCompletedEvent;
import com.atlassian.crucible.event.CommentCreatedEvent;
import com.atlassian.crucible.event.ReviewCompletionEvent;
import com.atlassian.crucible.event.ReviewStateChangedEvent;
import com.atlassian.crucible.spi.PermId;
import com.atlassian.crucible.spi.data.CommentData;
import com.atlassian.crucible.spi.data.DetailedReviewData;
import com.atlassian.crucible.spi.data.ReviewData;
import com.atlassian.crucible.spi.data.ReviewData.State;
import com.atlassian.crucible.spi.data.UserData;
import com.atlassian.crucible.spi.services.ReviewService;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.google.gson.Gson;

import java.util.List;

public class ReviewListener {
    private final ReviewService reviewService;
    private final SlackSettingsStore slackSettingsStore;
    private final SlackSender slackSender;

    public ReviewListener(ReviewService reviewService, SlackSettingsStore slackSettingsStore, SlackSender slackSender, EventPublisher eventPublisher) {
        this.reviewService = reviewService;
        this.slackSettingsStore = slackSettingsStore;
        this.slackSender = slackSender;
        eventPublisher.register(this);
    }

    @EventListener
    public void reviewCreated(ReviewStateChangedEvent reviewCreatedEvent) {
        if (reviewCreatedEvent.getNewState() == State.Review) {
            ReviewData reviewData = this.reviewService.getReview(reviewCreatedEvent.getReviewId());
            SlackChannelSettings settings = this.getChannelSettings(reviewData.getProjectKey());
            String id1 = reviewData.getPermaId().getId();
            String text1 = reviewData.getAuthor().getUserName() + " started review " + SlackMessage.url(settings.getBaseUrl() + id1, id1);
            SlackMessage slackMessage1 = new SlackMessage();
            slackMessage1.setText(text1);
            this.slackSender.sendMessage(slackMessage1, settings);
        }
    }

    @EventListener
    public void allReviewerCompletedEvent(AllReviewersCompletedEvent allReviewersCompletedEvent) {
        PermId id = allReviewersCompletedEvent.getReviewId();
        DetailedReviewData reviewData = this.reviewService.getReviewDetails(id);
        SlackChannelSettings settings = this.getChannelSettings(reviewData.getProjectKey());
        SlackMessage slackMessage = new SlackMessage();
        slackMessage.setText("Review completed " + SlackMessage.url(settings.getBaseUrl() + id.getId(), id.getId()));
        this.slackSender.sendMessage(slackMessage, settings);
    }

    // Send message to channel when new comment is added.
    @EventListener
    public void newCommentAdded(CommentCreatedEvent commentCreatedEvent) {
        CommentData commentData = this.reviewService.getComment(commentCreatedEvent.getCommentId());
        ReviewData reviewData = this.reviewService.getReview(commentCreatedEvent.getReviewId());
        SlackChannelSettings settings = this.getChannelSettings(reviewData.getProjectKey());
        SlackMessage slackMessage = this.createNewComment(commentData.getPermaIdAsString(), commentData.getUser(), commentData.getMessage(), settings);
        if (slackMessage != null) {
            this.slackSender.sendMessage(slackMessage, settings);
        }
    }

    // Send message when someone completes a review.
    @EventListener
    public void reviewEventChange(ReviewCompletionEvent reviewCompletionEvent) {
        ReviewData reviewData = this.reviewService.getReview(reviewCompletionEvent.getReviewId());
        SlackChannelSettings settings = this.getChannelSettings(reviewData.getProjectKey());
        String id1 = reviewData.getPermaId().getId();
        String user = reviewCompletionEvent.getActioner().getUserName();
        SlackMessage slackMessage = new SlackMessage();
        slackMessage.setText(user + " updated their review status for " + SlackMessage.url(settings.getBaseUrl() + id1, id1));
        this.slackSender.sendMessage(slackMessage, settings);
    }

    // Create the comment message.
    private SlackMessage createNewComment(String commentId, UserData from, String message, SlackChannelSettings settings) {
        String fromSlack = from.getUserName();
        if (fromSlack != null) {
            String text = fromSlack + " : " + message + " " + SlackMessage.url(settings.getBaseUrl() + commentId, commentId);
            SlackMessage slackMessage = new SlackMessage();
            slackMessage.setText(text);
            return slackMessage;
        } else {
            return null;
        }
    }

    // Get the Channel's Settings.
    private SlackChannelSettings getChannelSettings(String projectKey) {
        SlackSettings slackSettings = this.slackSettingsStore.getSettings();
        SlackChannels slackChannels = (new Gson()).fromJson(slackSettings.getChannelSettings(), SlackChannels.class);
        List<Channel> channelList = slackChannels.getChannels();
        SlackChannelSettings slackChannelSettings = new SlackChannelSettings();
        for (Channel tmpChannel : channelList) {
            if (tmpChannel.getProject().equals(projectKey)) {
                // SEt the slackChannelSettings
                slackChannelSettings.setBaseUrl(tmpChannel.getBaseUrl());
                slackChannelSettings.setWebhooks(tmpChannel.getWebhook());
            }
        }
        return slackChannelSettings;
    }
}
