package com.sitehands.crucible_plugins.slack;

import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class SlackServlet extends HttpServlet {
    private final TemplateRenderer templateRenderer;
    private final SlackSettingsStore store;
    private final SlackSender slackSender;

    public SlackServlet(TemplateRenderer templateRenderer, SlackSettingsStore store, SlackSender slackSender) {
        this.templateRenderer = templateRenderer;
        this.store = store;
        this.slackSender = slackSender;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setAttribute("decorator", "fisheye.userprofile.tab");
        response.setContentType("text/html");
        HashMap params = new HashMap();
        SlackSettings settings = this.store.getSettings();
        if (settings == null) {
            settings = new SlackSettings("");
        }

        params.put("slackSettings", settings);
        this.templateRenderer.render("templates/config.vm", params, response.getWriter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String channelSettings = request.getParameter("channelSettings");
        SlackSettings settings = new SlackSettings(channelSettings);
        response.sendRedirect("./slack-servlet");
    }
}
